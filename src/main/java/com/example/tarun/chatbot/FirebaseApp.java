package com.example.tarun.chatbot;

import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Tarun Gupta
 */
public class FirebaseApp extends android.app.Application {

    @Override
    public void onCreate() {
        super.onCreate();
        com.google.firebase.FirebaseApp.initializeApp(this);
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }
}

